ls=[10,20,30,87,45]
try:
    print('Try Block')
    print(ls[1]/0)
    # print(ls[5])

except IndexError:
    print('Except Block')
    print('Index Not Exist, Maximum Index: ',len(ls)-1)

except ZeroDivisionError:
    # print('Except Zero')
    print('number Cannot divided by 0')